<?php
define('FREEBASE_ENDPOINT', 'http://www.freebase.com/api/service/');

/**
 * STUB
 */
function freebase_search($query_string) {
//   drupal_set_message(t("Searching freebase.com for %query_string", array('%query_string' => $query_string)));
  $url = FREEBASE_ENDPOINT . 'search?mql_output=' . urlencode('[{"mid":null,"name":null,"type":[{"id":null,"name":null}]}]') . '&query='. urlencode($query_string);
// $url = FREEBASE_ENDPOINT . 'search?mql_output=' . urlencode('[{"id":null,"mid":null,"name":null,"type":[{"id":null,"name":null}],"/common/topic/article":{}}]') . '&query='. urlencode($query_string);
  $result = drupal_http_request($url);
  
  if (intval($result->code) != 200) {
    drupal_set_message(t('Error querying freebase. %errormessage' , array('%errormessage' => $result->error)), 'error');
  }
//   drupal_set_message(nl2br(print_r($result->code, 1)));

//   return nl2br(print_r($result, 1));
  return freebase_json_decode($result->data);
}

/**
 * Get an object
 */
function freebase_read($mid, $fields = array('name', 'type', 'mid')) {

  foreach($fields as $f) {
    $query .= ',"' . $f  .'":[]';
  }
  $url = 'http://api.freebase.com/api/service/mqlread?query='. urlencode('{"query":[{"mid":"' . $mid .'"' . $query .'}]}'  );

  $result = drupal_http_request($url);
  
  if (intval($result->code) != 200) {
    drupal_set_message(t('Error querying freebase. %errormessage' , array('%errormessage' => $result->error)), 'error');
  }
  return freebase_json_decode($result->data);
}

/**
 * STUB
 */
function freebase_write() {

}

/**
 * decodes the string that came from freebase.com as json
 * This code stolen from drupal 7
 * WIBBLE BLIBBLE BLIBBLE
 */
function freebase_json_decode($var) {
  return json_decode($var, TRUE);
}